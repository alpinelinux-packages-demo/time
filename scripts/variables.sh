# export ELAPSED_TIME="$(
    # printf '\e[90;47m%%e\e[0m')" # Unused?
export ELAPSED_TIME="$(gw '%e')" # Unused?
export REPOSITORY='http://dl-cdn.alpinelinux.org/alpine/edge/testing'
export SLEEP=0 # .7
export TERM="xterm-color"
# export TIME="$(
    # printf 'e:\e[90;47m%%e\e[0m U:%%U S:%%S seconds I:%%I O:%%O M:%%M')"
export TIME="e:$(gw '%e') U:%U S:%S seconds I:%I O:%O M:%M"
export TIME4BUSYBOX='    I:%I    O:%O    M:%M'
# export TIMEFORMAT="$(
    # printf '\e[90;47m%%R\e[0m seconds')"
export TIMEFORMAT="$(gw '%R') seconds (TIMEFORMAT)"
#^ for bash time
